package bll;

import java.util.List;
import java.util.NoSuchElementException;



import dao.ClientDao;
import model.Client;

public class ClientBll {

	

	public Client findClientById(int id) {
		Client client = ClientDao.findById(id);
		if (client == null) {
			throw new NoSuchElementException("Clientul cu ID " + id + " nu a fost gasit!");
		}
		return client;
	}

	public int insertClient(Client client) {
		return ClientDao.insert(client);
	}
		
	public void deleteClient(int id) {
		Client client = ClientDao.findById(id);
		if (client == null) {
			throw new NoSuchElementException("Clientul cu ID " + id + " nu a fost gasit!");
		} else {
			ClientDao obj = new ClientDao();
			obj.delete(client.getId());
		}
	}
	
	public void updateClient(Client client) {
		Client cl = ClientDao.findById(client.getId());
		if (cl == null) {
			throw new NoSuchElementException("Clientul cu ID " + client.getId()+ " nu a fost gasit!");
		} else {
			
			ClientDao.update(client);
		}
	}
	
	public List<Object> selectAllClients() {
	 ClientDao obj2= new ClientDao();
				return obj2.selectAll();
	}

}
