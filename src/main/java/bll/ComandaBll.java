package bll;
import java.util.List;
import java.util.NoSuchElementException;



import dao.ComandaDao;
import model.Comanda;

public class ComandaBll {

	

	public Comanda findComandaById(int comandaId) {
		Comanda comanda = ComandaDao.findById(comandaId);
		if (comanda == null) {
			throw new NoSuchElementException("Comandaul cu ID " + comandaId + " nu a fost gasit!");
		}
		return comanda;
	}

	public int insertComanda(Comanda comanda) {
		return ComandaDao.insert(comanda);
	}
		

	
	public List<Object> selectAllComanda() {
	 ComandaDao obj2= new ComandaDao();
				return obj2.selectAll();
	}

}
