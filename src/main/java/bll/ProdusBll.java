package bll;


import java.util.List;
import java.util.NoSuchElementException;



import dao.ProdusDao;
import model.Produs;

public class ProdusBll {


	public Produs findProdusById(int id) {
		Produs prod = ProdusDao.findById(id);
		if (prod == null) {
			throw new NoSuchElementException("Produsul cu ID " + id + " nu a fost gasit!");
		}
		return prod;
	}

	public int insertProdus(Produs prod) {
	
		return ProdusDao.insert(prod);
	}
		
	public void deleteProdus(int id) {
		
		if (ProdusDao.findById(id) == null) {
			throw new NoSuchElementException("Produsul cu ID " + id + " nu a fost gasit!");
		} else {
			ProdusDao.delete(ProdusDao.findById(id).getId());
		}
	}
	
	public void updateProdus(Produs prod) {
		Produs p = ProdusDao.findById(prod.getId());
		if (p == null) {
			throw new NoSuchElementException("Produsul cu ID " + prod.getId() + " nu a fost gasit!");
		
		} else {
			
			ProdusDao.update(prod);
		}
	}
	
	public List<Object> selectAllProdus() {
		return ProdusDao.selectAll();
	}

}
