package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import model.Client;

public class ClientDao {
	
	protected static final Logger LOGGER = Logger.getLogger(ClientDao.class.getName());
	private static final String insertStatementString = "INSERT INTO client (id, nume, email, telefon) VALUES (?, ?, ?, ?);";
	private final static String findStatementString = "SELECT * FROM client where id = ?;";
	private final static String deleteStatementString = "DELETE FROM client WHERE id = ?";
	private final static String updateStatementString = "UPDATE client SET nume = ?, email = ?, telefon = ? WHERE id = ?;";
	private final static String selectAllStatementString = "SELECT * FROM client;";
	
	public static Client findById(int clientId) {
		Client toReturn = null;
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, clientId);
			rs = findStatement.executeQuery();
			rs.next();
			
			String name = rs.getString("nume");
			String email = rs.getString("email");
			String phoneNumber = rs.getString("telefon");
			toReturn = new Client(clientId, name, email, phoneNumber);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ClientDao:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		
		return toReturn;
	}
	
	public static int insert(Client client) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement insertStatement = null;
		int insertedId = -1;
		
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, client.getId());
			insertStatement.setString(2, client.getNume());
			insertStatement.setString(3, client.getEmail());
			insertStatement.setString(4, client.getTelefon());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ClientDao:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		
		return insertedId;
	}

	public void delete(int clientId) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement deleteStatement = null;
		
		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementString);
			deleteStatement.setInt(1, clientId);
			deleteStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ClientDao:delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
	
	public static void update(Client client) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement = null;
		
		try {
			updateStatement = dbConnection.prepareStatement(updateStatementString);
			updateStatement.setString(1, client.getNume());
			updateStatement.setString(2, client.getEmail());
			updateStatement.setString(3, client.getTelefon());
			updateStatement.setInt(4, client.getId());
			updateStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ClientDao:update " + e.getMessage());
		} finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
	}

	public  List<Object> selectAll() {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement selectAllStatement = null;
		ResultSet rs = null;
		List<Object> clients = new ArrayList<Object>();
		
		try {
			selectAllStatement = dbConnection.prepareStatement(selectAllStatementString);
			rs = selectAllStatement.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("id");
				String nume = rs.getString("nume");
				String email = rs.getString("email");
				String telefon = rs.getString("telefon");
				clients.add(new Client(id, nume, email, telefon));
			}	
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ClientDao:selectAll " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(selectAllStatement);
			ConnectionFactory.close(dbConnection);
		} 
		
		return clients;
	}
	

}
