package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import model.Comanda;

public class ComandaDao {
	
	protected static final Logger LOGGER = Logger.getLogger(ComandaDao.class.getName());
	private static final String insertStatementString = "INSERT INTO comanda (idComanda, idClient, idProdus, cantitate) VALUES (?, ?, ?, ?);";
	private final static String findStatementString = "SELECT * FROM comanda where idComanda = ?;";
	//private final static String deleteStatementString = "DELETE FROM comanda WHERE id = ?";
	//private final static String updateStatementString = "UPDATE comanda SET idClient = ?, idProdus = ?, cantitate = ? WHERE idComanda = ?;";
	private final static String selectAllStatementString = "SELECT * FROM comanda;";
	
	public static Comanda findById(int comandaId) {
		Comanda toReturn = null;
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, comandaId);
			rs = findStatement.executeQuery();
			rs.next();
			
			int idC = rs.getInt("idClient");
			int idP = rs.getInt("idProdus");
			int cant = rs.getInt("cantitate");
			
			toReturn = new Comanda(comandaId, idC, idP, cant);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ComandaDao:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		
		return toReturn;
	}
	
	public static int insert(Comanda comanda) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement insertStatement = null;
		int insertedId = -1;
		
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, comanda.getIdComanda());
			insertStatement.setInt(2, comanda.getIdClient());
			insertStatement.setInt(3, comanda.getIdProduct());
			insertStatement.setInt(4, comanda.getCantitate());
			
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ComandaDao:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		
		return insertedId;
	}

	

	public  List<Object> selectAll() {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement selectAllStatement = null;
		ResultSet rs = null;
		List<Object> comenzi = new ArrayList<Object>();
		
		try {
			selectAllStatement = dbConnection.prepareStatement(selectAllStatementString);
			rs = selectAllStatement.executeQuery();
			while (rs.next()) {
				int idCom = rs.getInt("idComanda");
				int idC = rs.getInt("idClient");
				int idP = rs.getInt("idProdus");
				int cant = rs.getInt("cantitate");
			
				comenzi.add(new Comanda(idCom,idC, idP, cant));
			}	
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ComandaDao:selectAll " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(selectAllStatement);
			ConnectionFactory.close(dbConnection);
		} 
		
		return comenzi;
	}
	

}

