package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import model.Produs;

public class ProdusDao {
	
	protected static final Logger LOGGER = Logger.getLogger(ProdusDao.class.getName());
	private static final String insertStatementString = "INSERT INTO produs (id, nume, pret, cantitate) VALUES (?, ?, ?, ?);";
	private final static String findStatementString = "SELECT * FROM produs where id = ?;";
	private final static String deleteStatementString = "DELETE FROM produs WHERE id = ?";
	private final static String updateStatementString = "UPDATE produs SET nume = ?, pret = ?, cantitate = ? WHERE id = ?;";
	private final static String selectAllStatementString = "SELECT * FROM produs;";
	
	public static Produs findById(int produsId) {
		Produs toReturn = null;
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setInt(1, produsId);
			rs = findStatement.executeQuery();
			rs.next();

			String nume = rs.getString("nume");
			int price = rs.getInt("pret");
			int cantitate = rs.getInt("cantitate");
			toReturn = new Produs(produsId, nume, price, cantitate);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ProdusDao:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		
		return toReturn;
	}
	
	public static int insert(Produs produs) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement insertStatement = null;
		int insertedId = -1;
		
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1,  produs.getId());
			insertStatement.setString(2, produs.getNume());
			insertStatement.setInt(3, produs.getPret());
			insertStatement.setInt(4, produs.getCantitate());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProdusDao:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		
		return insertedId;
	}
	
	public static void delete(int produsId) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement deleteStatement = null;
		
		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementString);
			deleteStatement.setInt(1, produsId);
			deleteStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ProdusDao:delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
	}

	public static void update(Produs produs) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement = null;
		
		try {
			updateStatement = dbConnection.prepareStatement(updateStatementString);
			updateStatement.setString(1, produs.getNume());
			updateStatement.setInt(2, produs.getPret());
			updateStatement.setInt(3, produs.getCantitate());
			updateStatement.setInt(4, produs.getId());
			updateStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ProdusDao:update " + e.getMessage());
		} finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
	
	public static List<Object> selectAll() {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement selectAllStatement = null;
		ResultSet rs = null;
		List<Object> produs = new ArrayList<Object>();
		
		try {
			selectAllStatement = dbConnection.prepareStatement(selectAllStatementString);
			rs = selectAllStatement.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("id");
				String nume = rs.getString("nume");
				int pret = rs.getInt("pret");
				int cantitate = rs.getInt("cantitate");
				produs.add(new Produs(id, nume, pret, cantitate));
			}	
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ProdusDao:selectAll " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(selectAllStatement);
			ConnectionFactory.close(dbConnection);
		} 
		
		return produs;
	}
	
}
