package model;

public class Client {

	private int id;
	private String nume;
	private String email;
	private String telefon;
	
	public Client() {
		
	}

	/*public Client(String nume, String email, String telefon) {
		this.nume = nume;
		this.email = email;
		this.telefon = telefon;
	}*/
	
	public Client(int id, String nume, String email, String telefon) {
		this.id = id;
		this.nume = nume;
		this.email = email;
		this.telefon = telefon;
	}
	
	public int getId() {
		return id;
	}
	
	public String getNume() {
		return nume;
	}
	
	public String getEmail() {
		return email;
	}
	
	public String getTelefon() {
		return telefon;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setNume(String nume) {
		this.nume = nume;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}
	
	public String toString() {
		return "Client [ID = " + id + ", nume = " + nume + ", email = " + email + ", telefon = " + telefon + "]";
	}
	
}

