package model;

public class Comanda {
	private int idComanda;
	private int idClient;
	private int idProduct;
	private int cantitate;
	
	public Comanda() {
		
	}
	
	public Comanda(int idComanda, int idClient, int idProduct, int cantitate) {
		this.idComanda = idComanda;
		this.idClient = idClient;
		this.idProduct = idProduct;
		this.cantitate = cantitate;
	}
	public int getIdComanda() {
		return idComanda;
	}
	public int getIdClient() {
		return idClient;
	}
	
	public int getIdProduct() {
		return idProduct;
	}
	
	public int getCantitate() {
		return cantitate;
	}
	
	public void setIdComanda(int idComanda) {
		this.idComanda = idComanda;
	}
	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}
	
	public void setIdProduct(int idProduct) {
		this.idProduct = idProduct;
	}
	
	public void setCantitate(int cantitate) {
		this.cantitate = cantitate;
	}
}
