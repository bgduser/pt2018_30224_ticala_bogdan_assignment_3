package model;

public class Produs {

	private int id;
	private String nume;
	private int pret;
	private int cantitate;

	public Produs() {
		
	}
	
	public Produs(String nume, int pret, int cantitate) {
		this.pret = pret;
		this.nume = nume;
		this.cantitate = cantitate;
	}
	
	public Produs(int id, String nume, int pret, int cantitate) {
		this.id = id;
		this.pret = pret;
		this.nume = nume;
		this.cantitate = cantitate;
	}
	
	public int getId() {
		return id;
	}
	
	public int getPret() {
		return pret;
	}
	
	public String getNume() {
		return nume;
	}
	
	public int getCantitate() {
		return cantitate;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setPret(int pret) {
		this.pret = pret;
	}
	
	public void setNume(String nume) {
		this.nume = nume;
	}
	
	public void setCantitate(int cantitate) {
		this.cantitate = cantitate;
	}
}
