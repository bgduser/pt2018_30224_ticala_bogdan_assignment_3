package view;


import javax.swing.*;

import bll.ClientBll;
import model.Client;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AddNewClient extends JFrame {

	private static final long serialVersionUID = 1L;

	

	private JTextField numeTF;
	private JTextField emailTF;
	private JTextField telefonTF;

	private JButton cancelBtn;
	private JButton addClientBtn;
	
	private JLabel numeLbl;
	private JLabel emailLbl;
	private JLabel telefonLbl;

	private JPanel panel;



	private JLabel idLbl;



	private JTextField idTF;

	public AddNewClient() {
		panel = new JPanel();

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(400, 300);
		setTitle("Adaugare client");

		panel.setLayout(null);
		add(panel);
		
		idLbl = new JLabel("Id:");
		idLbl.setBounds(20, 110, 150, 20);
		panel.add(idLbl);
		
		idTF= new JTextField();
		idTF.setBounds(180, 110, 150, 20);
		panel.add(idTF);
		
		
		numeLbl = new JLabel("Nume:");
		numeLbl.setBounds(20, 20, 150, 20);
		panel.add(numeLbl);

		numeTF = new JTextField();
		numeTF.setBounds(180, 20, 150, 20);
		panel.add(numeTF);

		emailLbl = new JLabel("E-mail:");
		emailLbl.setBounds(20, 50, 150, 20);
		panel.add(emailLbl);

		emailTF = new JTextField();
		emailTF.setBounds(180, 50, 150, 20);
		panel.add(emailTF);

		telefonLbl = new JLabel("Telefon:");
		telefonLbl.setBounds(20, 80, 150, 20);
		panel.add(telefonLbl);

		telefonTF = new JTextField();
		telefonTF.setBounds(180, 80, 150, 20);
		panel.add(telefonTF);

		cancelBtn = new JButton("Cancel");
		cancelBtn.setBounds(180,160,150,30);
		panel.add(cancelBtn);

		addClientBtn = new JButton("Add");
		addClientBtn.setBounds(20, 160, 150, 30);
		panel.add(addClientBtn);

		cancelBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				dispose();
				new ClientOp();
			}
		});

		addClientBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				dispose();
				try {
					ClientBll clientBLL = new ClientBll();
					int add = clientBLL.insertClient(new Client( Integer.parseInt(idTF.getText()), numeTF.getText(), emailTF.getText(), telefonTF.getText()));
					if (add > 0) {
						JOptionPane.showMessageDialog(null, "Succes!", null,
								JOptionPane.PLAIN_MESSAGE);
						new ClientOp();
					} else {
						JOptionPane.showMessageDialog(null, "An error has occurred. Please try again!", null,
								JOptionPane.ERROR_MESSAGE);
						new ClientOp();
					}
				} catch (IllegalArgumentException e) {
					JOptionPane.showMessageDialog(null, "Wrong input! Please try again!", null,
							JOptionPane.ERROR_MESSAGE);
					new ClientOp();
				}
			}
		});

		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
	}

}
