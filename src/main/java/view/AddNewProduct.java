package view;



import javax.swing.*;

import bll.ProdusBll;
import model.Produs;

import java.awt.event.ActionListener;

import java.awt.event.ActionEvent;

public class AddNewProduct extends JFrame {

	private static final long serialVersionUID = 1L;

	private JLabel numeLbl;
	private JLabel pretLbl;
	private JLabel cantLbl;

	private JTextField numeTF;
	private JTextField pretTF;
	private JTextField cantTF;

	private JButton cancelBtn;
	private JButton addProductBtn;

	private JPanel panel;

	private JLabel idLbl;

	private JTextField idTF;

	public AddNewProduct() {
		
		panel = new JPanel();

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(400, 300);
		setTitle("Adaugare produs");
		
		panel.setLayout(null);
		
		add(panel);
		
		idLbl = new JLabel("Id:");
		idLbl.setBounds(20, 110, 150, 20);
		panel.add(idLbl);
		
		idTF= new JTextField();
		idTF.setBounds(180, 110, 150, 20);
		panel.add(idTF);
		
		numeLbl = new JLabel("Nume:");
		numeLbl.setBounds(20, 20, 150, 20);
		
		panel.add(numeLbl);

		pretTF = new JTextField();
		pretTF.setBounds(180, 50, 150, 20);
		panel.add(pretTF);

		cantLbl = new JLabel("Cantitate:");
		cantLbl.setBounds(20, 80, 150, 20);
		
		panel.add(cantLbl);

		cantTF = new JTextField();
		cantTF.setBounds(180, 80, 150, 20);
		panel.add(cantTF);
		
		numeTF = new JTextField();
		numeTF.setBounds(180, 20, 150, 20);
		panel.add(numeTF);

		pretLbl = new JLabel("Pret:");
		pretLbl.setBounds(20, 50, 150, 20);
	
		panel.add(pretLbl);

		

		cancelBtn = new JButton("Cancel");
		cancelBtn.setBounds(180,160,150,30);
		panel.add(cancelBtn);

		addProductBtn = new JButton("Add");
		addProductBtn.setBounds(20, 160, 150, 30);

		panel.add(addProductBtn);

		cancelBtn.addActionListener(new ActionListener() {
		
			public void actionPerformed(ActionEvent ae) {
				dispose();
				new ProdusOp();
			}
		});

		addProductBtn.addActionListener(new ActionListener() {
		
			public void actionPerformed(ActionEvent ae) {
				dispose();
				try {
					ProdusBll productBLL = new ProdusBll();
					int id = productBLL.insertProdus(new Produs(Integer.parseInt(idTF.getText()),numeTF.getText(), Integer.parseInt(pretTF.getText()),
							Integer.parseInt(cantTF.getText())));
					if (id > 0) {
						JOptionPane.showMessageDialog(null, "Succes!", null,
								JOptionPane.PLAIN_MESSAGE);
						new ProdusOp();
					} else {
						JOptionPane.showMessageDialog(null, "An error has occurred. Please try again!", null,
								JOptionPane.ERROR_MESSAGE);
						new ProdusOp();
					}
				} catch (IllegalArgumentException e) {
					JOptionPane.showMessageDialog(null, "Wrong input! Please try again!", null,
							JOptionPane.ERROR_MESSAGE);
					new ProdusOp();
				}
			}
		});

		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
	}

}
