package view;

import bll.ComandaBll;
import bll.ProdusBll;

import model.Comanda;
import model.Produs;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.NoSuchElementException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import javax.swing.JTextField;


public class AddOrder  extends JFrame {
	
	
	private static final long serialVersionUID = 1L;
	
	private JLabel clientIdLbl;
	private JLabel produsIdLbl;
	private JLabel produsCantitateLbl;

	private JTextField clientIdTF;
	private JTextField produsIdTF;
	private JTextField produsCantitateTF;

	private JButton addOrderBtn;
	private JButton cancelBtn;

	private JTextField comandaIdTF;
	private JLabel comandaIdLbl;

	private JPanel panel;
	
	
	
	
	public AddOrder() {
	
		panel = new JPanel();

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(400, 300);
		setTitle("Comanda");

		panel.setLayout(null);
		add(panel);	
	

	comandaIdLbl = new JLabel("Comanda ID:");
	comandaIdLbl.setBounds(20, 20, 150, 20);
	panel.add(comandaIdLbl);
	
	clientIdLbl = new JLabel("Client ID:");
	clientIdLbl.setBounds(20, 50, 150, 20);
	panel.add(clientIdLbl);
	
	produsIdLbl = new JLabel("Produs ID:");
	produsIdLbl.setBounds(20,80,150,20);
	panel.add(produsIdLbl);
	
	produsCantitateLbl = new JLabel("Cantitate produs:");
	produsCantitateLbl.setBounds(20, 110, 150, 20);
	panel.add(produsCantitateLbl);
	
	comandaIdTF = new JTextField();
	comandaIdTF.setBounds(180,20,150,20);
	panel.add(comandaIdTF);

	clientIdTF = new JTextField();
	clientIdTF.setBounds(180, 50, 150, 20);
	panel.add(clientIdTF);

	produsIdTF = new JTextField();
	produsIdTF.setBounds(180, 80, 150, 20);
	panel.add(produsIdTF);

	
	produsCantitateTF = new JTextField();
	produsCantitateTF.setBounds(180, 110, 150, 20);
	panel.add(produsCantitateTF);

	
	
	cancelBtn = new JButton("Cancel");
	cancelBtn.setBounds(20, 140, 150, 30);
	
	panel.add(cancelBtn);

	addOrderBtn = new JButton("Plasare");
	addOrderBtn.setBounds(180, 140, 150, 30);
	
	panel.add(addOrderBtn);
	

	cancelBtn.addActionListener(new ActionListener() {
	
		public void actionPerformed(ActionEvent ae) {
			dispose();
			new Interfata();
		}
	});
	
	
	addOrderBtn.addActionListener(new ActionListener() {

		public void actionPerformed(ActionEvent ae) {
			dispose();

			try {
				ProdusBll productBLL = new ProdusBll();
				
				ComandaBll comandaBll = new ComandaBll();
				Produs prod = productBLL.findProdusById(Integer.parseInt(produsIdTF.getText()));
				
				
				int produsID = Integer.parseInt(produsIdTF.getText());
				int pdId = productBLL.findProdusById(produsID).getId();
				
				if (Integer.parseInt(produsCantitateTF.getText()) > productBLL.findProdusById(pdId).getCantitate()) {
					JOptionPane.showMessageDialog(null, "Stoc indisponibil!", null,
							JOptionPane.ERROR_MESSAGE);
					new AddOrder();
					
			}
				else {
				
					comandaBll.insertComanda(new Comanda(Integer.parseInt(comandaIdTF.getText()) ,Integer.parseInt(clientIdTF.getText()),
							Integer.parseInt(produsIdTF.getText()), 
							Integer.parseInt(produsCantitateTF.getText())));
					
					productBLL.findProdusById(pdId).setCantitate(prod.getCantitate()-Integer.parseInt(produsCantitateTF.getText()));	
					
					productBLL.updateProdus(prod);
					
					JOptionPane.showMessageDialog(null, "Comanda a fost plasata!", null,
							JOptionPane.PLAIN_MESSAGE);
					
					
					
				}
			}
			catch (NoSuchElementException e) {
				JOptionPane.showMessageDialog(null, "Nu s-a gasit client sau produs cu acest ID", null,
						JOptionPane.ERROR_MESSAGE);
				new Order();
			}

		}
	});

	setResizable(false);
	setLocationRelativeTo(null);
	setVisible(true);
}




}


