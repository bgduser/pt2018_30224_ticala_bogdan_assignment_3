package view;



import javax.swing.*;

import bll.ClientBll;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;



public class ClientOp extends JFrame {

	private static final long serialVersionUID = 1L;

	private JButton addNewClientBtn;
	private JButton editClientBtn;
	private JButton deleteClientBtn;
	private JButton backBtn;
	
	private JTable clientsTable;
	
	private JPanel panel;
	
	public ClientOp() {
		panel = new JPanel();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(670, 500);
		setTitle("Operatii client");
		
		panel.setLayout(null);
	
		add(panel);
		
		addNewClientBtn = new JButton("Adaugare client");
		addNewClientBtn.setBounds(20, 20, 150, 30);
		
		panel.add(addNewClientBtn);
		
		editClientBtn = new JButton("Editare client");
		editClientBtn.setBounds(180, 20, 150, 30);
	
		panel.add(editClientBtn);
		
		deleteClientBtn = new JButton("Stergere client");
		deleteClientBtn.setBounds(340, 20, 150, 30);
	
		panel.add(deleteClientBtn);
		
		backBtn = new JButton("Back");
		backBtn.setBounds(500, 20, 150, 30);
		
		panel.add(backBtn);
		
		clientsTable = Reflection.createTable((new ClientBll()).selectAllClients()); 
		JScrollPane clientsSP = new JScrollPane(clientsTable);
		clientsSP.setBounds(20, 60, 630, 390);
		panel.add(clientsSP);
		
		backBtn.addActionListener(new ActionListener() {
		
			public void actionPerformed(ActionEvent ae) {
				dispose();
				new Interfata();
			}
		});
		
		addNewClientBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent ae) {
				dispose();
				new AddNewClient();
			}
		});
		
		editClientBtn.addActionListener(new ActionListener() {
		
			public void actionPerformed(ActionEvent ae) {
				dispose();
				new EditClient();
			}
		});
		
		deleteClientBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent ae) {
				dispose();
				new DeleteClient();
			}
		});
		
		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
}
