package view;


import javax.swing.*;

import bll.ClientBll;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.NoSuchElementException;

public class DeleteClient extends JFrame {

	private static final long serialVersionUID = 1L;
	
	private JLabel idLbl;
	private JTextField idTF;
	
	private JButton cancelBtn;
	private JButton deleteClientBtn;
	
	private JPanel panel;
	
	public DeleteClient() {
		panel = new JPanel();

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(350, 130);
		setTitle("Stergere client");

		panel.setLayout(null);
		add(panel);
		
		idLbl = new JLabel("Client ID:");
		idLbl.setBounds(20, 20, 150, 20);
		panel.add(idLbl);
		
		idTF = new JTextField();
		idTF.setBounds(180, 20, 150, 20);
		panel.add(idTF);
		
		cancelBtn = new JButton("Cancel");
		cancelBtn.setBounds(20, 50, 150, 30);
		panel.add(cancelBtn);
		
		deleteClientBtn = new JButton("Sterge client");
		deleteClientBtn.setBounds(180, 50, 150, 30);
		panel.add(deleteClientBtn);
		
		cancelBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				dispose();
				new ClientOp();
			}
		});
		
		deleteClientBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				dispose();
				ClientBll clientBLL = new ClientBll();
				try {
					clientBLL.deleteClient(Integer.parseInt(idTF.getText()));
					JOptionPane.showMessageDialog(null, "Successfully deleted!", null, JOptionPane.PLAIN_MESSAGE);
					new ClientOp();
				} catch (NoSuchElementException e) {
					JOptionPane.showMessageDialog(null, "No client with this ID found!", null, JOptionPane.ERROR_MESSAGE);
					new ClientOp();
				}
			}
		});
		
		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
}
