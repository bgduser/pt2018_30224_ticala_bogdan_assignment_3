package view;



import javax.swing.*;

import bll.ProdusBll;

import java.awt.event.ActionListener;
import java.util.NoSuchElementException;
import java.awt.event.ActionEvent;

public class DeleteProduct extends JFrame {

	private static final long serialVersionUID = 1L;

	private JLabel idLbl;
	private JTextField idTF;
	
	private JButton cancelBtn;
	private JButton deleteProductBtn;
	
	private JPanel panel;
	
	public DeleteProduct() {
		panel = new JPanel();

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(350, 130);
		setTitle("Stergere produs");

		panel.setLayout(null);
		add(panel);
		
		idLbl = new JLabel("Product ID:");
		idLbl.setBounds(20, 20, 150, 20);
		panel.add(idLbl);
		
		idTF = new JTextField();
		idTF.setBounds(180, 20, 150, 20);
		panel.add(idTF);
		
		cancelBtn = new JButton("Cancel");
		cancelBtn.setBounds(20, 50, 150, 30);
		panel.add(cancelBtn);
		
		deleteProductBtn = new JButton("Sterge produs");
		deleteProductBtn.setBounds(180, 50, 150, 30);
		panel.add(deleteProductBtn);
		
		cancelBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent ae) {
				dispose();
				new ProdusOp();
			}
		});
		
		deleteProductBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				dispose();
				ProdusBll productBLL = new ProdusBll();
				try {
					productBLL.deleteProdus(Integer.parseInt(idTF.getText()));
					JOptionPane.showMessageDialog(null, "Successfully deleted!", null, JOptionPane.PLAIN_MESSAGE);
					new ProdusOp();
				} catch (NoSuchElementException e) {
					JOptionPane.showMessageDialog(null, "No product with this ID found!", null, JOptionPane.ERROR_MESSAGE);
					new ProdusOp();
				}
			}
		});
		
		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
}
