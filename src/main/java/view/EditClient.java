package view;


import javax.swing.*;

import bll.ClientBll;
import model.Client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.NoSuchElementException;

public class EditClient extends JFrame {

	private static final long serialVersionUID = 1L;

	private JLabel idLbl;
	private JLabel nameLbl;
	private JLabel emailLbl;
	private JLabel phoneNumberLbl;

	private JTextField idTF;
	private JTextField nameTF;
	private JTextField emailTF;
	private JTextField phoneNumberTF;

	private JButton cancelBtn;
	private JButton editClientBtn;

	private JPanel panel;

	public EditClient() {
		panel = new JPanel();

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(350, 220);
		setTitle("Editare client");

		panel.setLayout(null);
		add(panel);
		
		idLbl = new JLabel("Client ID:");
		idLbl.setBounds(20, 20, 150, 20);
		panel.add(idLbl);
		
		idTF = new JTextField();
		idTF.setBounds(180, 20, 150, 20);
		panel.add(idTF);

		nameLbl = new JLabel("Nume nou:");
		nameLbl.setBounds(20, 50, 150, 20);
		panel.add(nameLbl);

		nameTF = new JTextField();
		nameTF.setBounds(180, 50, 150, 20);
		panel.add(nameTF);

		emailLbl = new JLabel("E-mail nou:");
		emailLbl.setBounds(20, 80, 150, 20);
		panel.add(emailLbl);

		emailTF = new JTextField();
		emailTF.setBounds(180, 80, 150, 20);
		panel.add(emailTF);

		phoneNumberLbl = new JLabel("Telefon nou:");
		phoneNumberLbl.setBounds(20, 110, 150, 20);
		panel.add(phoneNumberLbl);

		phoneNumberTF = new JTextField();
		phoneNumberTF.setBounds(180, 110, 150, 20);
		panel.add(phoneNumberTF);

		cancelBtn = new JButton("Cancel");
		cancelBtn.setBounds(20, 140, 150, 30);
		panel.add(cancelBtn);

		editClientBtn = new JButton("Editare client");
		editClientBtn.setBounds(180, 140, 150, 30);
		panel.add(editClientBtn);

		cancelBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				dispose();
				new ClientOp();
			}
		});

		editClientBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				dispose();
				try {
					ClientBll clientBLL = new ClientBll();
					clientBLL.updateClient(new Client(Integer.parseInt(idTF.getText()), nameTF.getText(), emailTF.getText(), phoneNumberTF.getText()));
					JOptionPane.showMessageDialog(null, "Successfully updated!", null, JOptionPane.PLAIN_MESSAGE);
					new ClientOp();
				} catch (IllegalArgumentException e) {
					JOptionPane.showMessageDialog(null, "Wrong input! Please try again!", null,
							JOptionPane.ERROR_MESSAGE);
					new ClientOp();
				} catch (NoSuchElementException e) {
					JOptionPane.showMessageDialog(null, "No client with this ID found!", null, JOptionPane.ERROR_MESSAGE);
					new ClientOp();
				}
			}
		});

		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
	}

}

