package view;


import javax.swing.*;

import bll.ProdusBll;
import model.Produs;

import java.awt.event.ActionListener;
import java.util.NoSuchElementException;
import java.awt.event.ActionEvent;

public class EditProduct extends JFrame {

	private static final long serialVersionUID = 1L;

	private JLabel idLbl;
	private JLabel nameLbl;
	private JLabel priceLbl;
	private JLabel qtyLbl;

	private JTextField idTF;
	private JTextField nameTF;
	private JTextField priceTF;
	private JTextField qtyTF;

	private JButton cancelBtn;
	private JButton editProductBtn;

	private JPanel panel;

	public EditProduct() {
		panel = new JPanel();

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(350, 220);
		setTitle("Editare produs");

		panel.setLayout(null);
		add(panel);

		idLbl = new JLabel("Product ID:");
		idLbl.setBounds(20, 20, 150, 20);
		panel.add(idLbl);
		
		idTF = new JTextField();
		idTF.setBounds(180, 20, 150, 20);
		panel.add(idTF);
		
		nameLbl = new JLabel("Nume nou:");
		nameLbl.setBounds(20, 50, 150, 20);
		panel.add(nameLbl);
		
		nameTF = new JTextField();
		nameTF.setBounds(180, 50, 150, 20);
		panel.add(nameTF);
		
		priceLbl = new JLabel("Pret nou:");
		priceLbl.setBounds(20, 80, 150, 20);
		panel.add(priceLbl);
		
		priceTF = new JTextField();
		priceTF.setBounds(180, 80, 150, 20);
		panel.add(priceTF);
		
		qtyLbl = new JLabel("Cantitate noua:");
		qtyLbl.setBounds(20, 110, 150, 20);
		panel.add(qtyLbl);
		
		qtyTF = new JTextField();
		qtyTF.setBounds(180, 110, 150, 20);
		panel.add(qtyTF);
		
		cancelBtn = new JButton("Cancel");
		cancelBtn.setBounds(20, 140, 150, 30);
		panel.add(cancelBtn);
		
		editProductBtn = new JButton("Editare produs");
		editProductBtn.setBounds(180, 140, 150, 30);
		panel.add(editProductBtn);
		
		cancelBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent ae) {
				dispose();
				new ProdusOp();
			}
		});
		
		editProductBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent ae) {
				dispose();
				try {
					ProdusBll productBLL = new ProdusBll();
					productBLL.updateProdus(new Produs(Integer.parseInt(idTF.getText()), nameTF.getText(), Integer.parseInt(priceTF.getText()), Integer.parseInt(qtyTF.getText())));
					JOptionPane.showMessageDialog(null, "Successfully updated!", null, JOptionPane.PLAIN_MESSAGE);
					new ProdusOp();
				} catch (IllegalArgumentException e) {
					JOptionPane.showMessageDialog(null, "Wrong input! Please try again!", null,
							JOptionPane.ERROR_MESSAGE);
					new ProdusOp();
				} catch (NoSuchElementException e) {
					JOptionPane.showMessageDialog(null, "No product with this ID found!", null, JOptionPane.ERROR_MESSAGE);
					new ProdusOp();
				}
			}
		});

		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
	}

}
