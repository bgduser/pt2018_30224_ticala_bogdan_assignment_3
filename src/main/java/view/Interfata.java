package view;


import javax.swing.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Interfata extends JFrame {
	
	private static final long serialVersionUID = 1L;
	
	private JButton clientBtn;
	private JButton comandaBtn;
	
	private JPanel panel;

	private JButton produsBtn;
	
	public Interfata() {	
		panel = new JPanel();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(670, 220);
		setTitle("Order manangement");
		
		panel.setLayout(null);
		add(panel);
		
		comandaBtn = new JButton("Comanda");
		comandaBtn.setBounds(20, 50, 200, 70);
		
		panel.add(comandaBtn);
		
		clientBtn = new JButton("Client");
		clientBtn.setBounds(230, 50, 200, 70);
		
		panel.add(clientBtn);
		
		produsBtn = new JButton("Produs");
		produsBtn.setBounds(440, 50, 200, 70);
		
		panel.add(produsBtn);
		
	
		
		comandaBtn.addActionListener(new ActionListener() {
		
			public void actionPerformed(ActionEvent ae) {
				dispose();
				new Order();
			}
		});
		
		clientBtn.addActionListener(new ActionListener() {
		
			public void actionPerformed(ActionEvent ae) {
				dispose();
				new ClientOp();
			}
		});
		
		produsBtn.addActionListener(new ActionListener() {
	
			public void actionPerformed(ActionEvent ae) {
				dispose();
				new ProdusOp();
			}
		});
	
		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
	}
	

	public static void main(String[] args) {
		new Interfata();
	}
	

}
