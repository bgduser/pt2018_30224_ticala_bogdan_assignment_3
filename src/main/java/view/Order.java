package view;

import javax.swing.*;



import bll.ComandaBll;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;



public class Order extends JFrame {
	
	private static final long serialVersionUID = 1L;

	private JButton addNewComandaBtn;
	
	private JButton backBtn;
	
	private JTable comandaTable;
	
	private JPanel panel;
	
	public Order() {
		panel = new JPanel();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(670, 500);
		setTitle("Operatii client");
		
		panel.setLayout(null);
	
		add(panel);
		
		addNewComandaBtn = new JButton("Adaugare comanda");
		addNewComandaBtn.setBounds(20, 20, 150, 30);
		
		panel.add(addNewComandaBtn);
		
		
		
		backBtn = new JButton("Back");
		backBtn.setBounds(500, 20, 150, 30);
		
		panel.add(backBtn);
		
		comandaTable = Reflection.createTable((new ComandaBll()).selectAllComanda()); 
		JScrollPane comandaSP = new JScrollPane(comandaTable);
		comandaSP.setBounds(20, 60, 630, 390);
		panel.add(comandaSP);
		
		backBtn.addActionListener(new ActionListener() {
		
			public void actionPerformed(ActionEvent ae) {
				dispose();
				new Interfata();
			}
		});
		
		addNewComandaBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent ae) {
				dispose();
				new AddOrder();
			}
		});
		
	
		
		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
	}
	 
	
	
}

	

	
