package view;


import javax.swing.*;

import view.Reflection;
import bll.ProdusBll;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ProdusOp extends JFrame {

	private static final long serialVersionUID = 1L;

	private JButton addNewProdusBtn;
	private JButton editProdusBtn;
	private JButton deleteProdusBtn;
	private JButton backBtn;
	
	private JTable productsTable;
	
	private JPanel panel;
	
	public ProdusOp() {
		panel = new JPanel();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(670, 500);
		setTitle("Operatii Produs");
		
		panel.setLayout(null);
		add(panel);
		
		addNewProdusBtn = new JButton("Adaugare produs");
		addNewProdusBtn.setBounds(20, 20, 150, 30);
		panel.add(addNewProdusBtn);
		
		deleteProdusBtn = new JButton("Stergere produs");
		deleteProdusBtn.setBounds(340, 20, 150, 30);
		panel.add(deleteProdusBtn);
		
		editProdusBtn = new JButton("Editare produs");
		editProdusBtn.setBounds(180, 20, 150, 30);
		panel.add(editProdusBtn);
		
		
		backBtn = new JButton("Back");
		backBtn.setBounds(500, 20, 150, 30);
		panel.add(backBtn);
		
		productsTable = Reflection.createTable((new ProdusBll()).selectAllProdus()); 
		JScrollPane productsSP = new JScrollPane(productsTable);
		productsSP.setBounds(20, 60, 630, 390);
		panel.add(productsSP);
		
		backBtn.addActionListener(new ActionListener() {
		
			public void actionPerformed(ActionEvent ae) {
				dispose();
				new Interfata();
			}
		});
		
		addNewProdusBtn.addActionListener(new ActionListener() {
	
			public void actionPerformed(ActionEvent ae) {
				dispose();
				new AddNewProduct();
			}
		});
		
		editProdusBtn.addActionListener(new ActionListener() {
	
			public void actionPerformed(ActionEvent ae) {
				dispose();
				new EditProduct();
			}
		});
		
		deleteProdusBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent ae) {
				dispose();
				new DeleteProduct();
			}
		});
		
		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
}

