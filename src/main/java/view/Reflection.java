package view;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class Reflection {
	
	public static JTable createTable(List<Object> objects) {
		if (objects.isEmpty()) {
			return new JTable();
		}
		
		DefaultTableModel dTable = new DefaultTableModel();
		Field[] array = objects.get(0).getClass().getDeclaredFields();
		for (Field field : array ) {
			dTable.addColumn(field.getName());
		}
		
		
		for (Object object : objects) {
			Vector<Object> rowData = new Vector<Object>();
			Field[] arr = object.getClass().getDeclaredFields();
			
			for (Field field : arr) {
				field.setAccessible(true);
				try {
					rowData.add(field.get(object));
				} catch (IllegalArgumentException | IllegalAccessException e) {
					e.printStackTrace();
				}
			}
			dTable.addRow(rowData);
		}
		
		JTable table = new JTable(dTable);
		return table;
	}

}
